<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

function exception_error_handler($errno, $errstr, $errfile, $errline )
{
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
};

function before($needle, $stack)
{
        return substr($stack, 0, strpos($stack, $needle));
};

function getUrlContent($url)
{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Nextcloud Updater');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($httpcode>=200 && $httpcode<300) ? $data : false;
};


set_error_handler("exception_error_handler");


require_once("/var/www/". $_GET["site"] ."/web/version.php");


$current = implode(".", $OC_Version);

$myurl = "https://updates.nextcloud.org/updater_server/?version=". implode("x", $OC_Version) ."xxx". $OC_Channel ."xx". urlencode($OC_Build) ."x". str_replace(".", "x", before("-", PHP_VERSION));
#$myurl = "https://updates.nextcloud.org/updater_server/?version=20x0x5x1xxxstablexx2021-01-25T15%3A50%3A36%2B00%3A00+fd26cb18701139187a14b6d6ef72ebe660b183a8x7x3x19";

$mydata = getUrlContent("$myurl");

if (!empty("$mydata"))
{
        $xml = simplexml_load_string("$mydata");
        $new = $xml->version;

        echo "Running Nextcloud (". $OC_Channel . ") version: ". $current .", new version: ". $new ." is available.\n";
         
        exit(1);
};

echo "Running Nextcloud (". $OC_Channel .") version: ". $current .", no new version available.\n";

exit(0);

?>
