#!/bin/bash

# heavily based on https://gist.github.com/cgmartin/49cd0aefe836932cdc9

#set -x

DOMAIN="$1";
PORT="$2"
DAYS=3;
TIMEZONE="Europe/Berlin"

case $PORT in
        mail)
                PORT=993
                WORD="MAIL"
                ;;
        *)
                PORT=443
                WORD="WEB"
esac

RETURN=$( openssl s_client -connect "${DOMAIN}":"${PORT}" -servername "${DOMAIN}" 2>/dev/null \
        | openssl x509 -text \
        | grep 'Not After' \
        | awk '{print $4,$5,$7}' )
CERTEXPDATE=$( date -d "${RETURN}" '+%s') 
WARNDATE=$(($(date +%s) + (86400*DAYS)))
EXPDATE=$( TZ="${TIMEZONE}" date --date=@"$CERTEXPDATE" )

case $PORT in
        mail)
                PORT=993
                ;;
        *)
                PORT=443
esac

if [ "$WARNDATE" -gt "$CERTEXPDATE" ]
then
    echo "ERROR - ${WORD}-Certificate for ${DOMAIN} expires in less than $DAYS days, on ${EXPDATE}" 
    exit 1
fi

echo "OK - ${WORD}-Certificate for ${DOMAIN} expires on ${EXPDATE}"
exit  0

